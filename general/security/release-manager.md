# Security Releases as a Release Manager
## General process overview

From a release manager standpoint, a non-critical security release is fairly
similar to a normal release, but with a few extra considerations in order to
avoid disclosing vulnerabilities before proper fixes are released. Additional
collaboration is needed to coordinate with the Security and Marketing teams.

## Critical

To be further defined.

## Non-critical
### What to include

A security release, even one for the latest monthly release, should _only_
include the changes necessary to resolve the security vulnerabilities. Including
fixes for regressions in a security patch increases the chances of breaking
something, both for users and for our packaging and release process.

The only exception to this policy is [release candidates]. If the monthly
release is in progress as we're preparing for a security release, it's
acceptable for a new RC to include both security fixes and regression fixes.
Care should be taken to coordinate the publishing of an RC package with the
other security patches so as to not disclose the security vulnerabilities
publicly before we're ready to disclose them.

<!--
> **TODO (rspeicher):** This is owned by the Security team now
Be sure NOT to pre-announce which GitLab versions are affected, since that may
allow malicious users to narrow the search space.
-->

[release candidates]: ../release-candidates.md

### Process

When working on a security release, it's important that all work happens
**only** on [dev.gitlab.org], so nothing is disclosed publicly before we intend
it.

You should run `scripts/security-harness` in the CE, EE, and Omnibus projects in
order to prevent accidental pushes to any remote other than `dev.gitlab.org`.

[dev.gitlab.org]: https://dev.gitlab.org/

#### Security branches

Developers [work off of security branches](developer.md#branches). As the
release manager, you'll be cherry-picking fixes from the `security-X-Y` branches
into the corresponding `X-Y-stable` branch when a fix is ready for a release.

`X-Y-stable` can regularly be merged into `security-X-Y` in order to keep it
up-to-date with normal changes, but _not the other way around_: `security-X-Y`
should never be merged into `X-Y-stable`, as a fix may have been merged that we
don't yet intend to release.

#### Steps:

1. Security Release Issue

    Someone from the security team will open the issue for the next security releases.

1. Merge Request that need to be picked

    The Release Manager will be assigned to the MRs that should be included.
    These MRs should already be reviewed, approved, and ready to be merged.

    For each scheduled issue, the Release Manager should be assigned to four Merge Request, one for master, one for the current release and two for the two latest releases.

    *Example:* If the current milestone is 10.8 you should open an MR targeting 10.8, 10.7 and 10.6 security branches, as well as one targeting `master`.

1. List the issues and Merge Requests that need to be included

    For each Merge Request that the Release manager was assigned to, list the issue and each merge request in the security issue.
    It will be helpful when it's time to cherry-pick them.

1. Merge the MRs assigned to the Release Mananger targeting `security-` branches

    It is the Release Manager responsibility to merge the MRs targeting the last 3 releases. **Do not merge the one targeting master yet**

1. For each release in the issue title, check which is the next minor release needed through `versions.gitlab.com`

1. Run `bundle exec rake "security_patch_issue[M.m.0]"` for each of them

1. Link the new issues as related issues

1. Follow the steps on the template for each of the created issues

1. Merge MRs targeting `master`

    After promoting the packages to public, the Release Manager should merge the MR targeting `master`. See more information about this step in [developer guide](developer.md#final-steps)

---

[Return to Security Guide](../security.md)
