## Run automated QA against the staging instance

After each deploy to staging, the automated QA tests should be run against staging to verify core functionality.

### Required information

* The current stable branch, we'll refer to this as STABLE_BRANCH
* The qa username and password from the `Team` vault in 1 password. This is under `GitLab QA`. We will refer to these as QA_USERNAME and QA_PASSWORD

### Running the tests

From the `qa` subdirectory of a `gitlab-ee` clone.

1. Ensure you are on the stable branch for the release: `git checkout STABLE_BRANCH`
1. Ensure you are up to date: `git pull`
1. Ensure the correct dependencies are installed: `bundle install`
1. Run `GITLAB_USERNAME=${QA_USERNAME} GITLAB_PASSWORD=${GITLAB_PASSWORD} bin/qa Test::Instance https://staging.gitlab.com`
1. Save the results as a snippet, and link to them in the `QA issue`.
1. If there are any failures, help track down the issue. Involve other developers as needed.
